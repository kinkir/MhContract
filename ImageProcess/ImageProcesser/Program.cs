﻿using System;
using System.Collections.Generic;

using System.Windows.Forms;
using PeerPublicLib;

namespace ImageProcesser
{
   public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        public static String CommandLineFileName = "";
       
        //主键
        public static Int32 CurHtID = 0;
        //合同编号
        public static string CurHtbh = "";
        //合同名称
        public static string CurHtmc = "";
        //年份
        public static string CurHtqdrq = "";
        
        public static void StartIns(){
           Application.Run(new ImageProcessorForm());
        }
        [STAThread]
       public static void Main(string [] args)
        {
            if (args!=null &&　args.Length >= 1)
            {
                CommandLineFileName = args[0];
            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ImageProcessorForm());
        }
    }
}
